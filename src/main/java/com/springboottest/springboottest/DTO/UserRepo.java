package com.springboottest.springboottest.DTO;

import com.springboottest.springboottest.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepo extends JpaRepository<User, Long>{
    List<User> findByNameOrAge(String name, int age);

    @Query(value = "SELECT * FROM User user WHERE user.id > :id", nativeQuery = true)
    List<User> findIds(@Param("id") Long id);
}
