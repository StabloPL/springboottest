package com.springboottest.springboottest.Service;

import com.springboottest.springboottest.DTO.UserRepo;
import com.springboottest.springboottest.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserService {
    private UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }


    public ArrayList<User> userGetAll(){
        return (ArrayList<User>) userRepo.findAll();
    }

    public User userGetByID(long id){
        Optional<User> userById = userRepo.findById(id);

        if (userById.isPresent())
        {
            return userById.get();
        }
        return null;
    }

    public boolean userPUT(long id, String name,String surname, int age){
        User user = userGetByID(id);
        if (user == null){
            return false;
        }
        if (null != name && !name.equals("")){
            user.setName(name);
        }
        if (null != surname && !surname.equals("")){
            user.setSurname(surname);
        }
        if (age >= 0){
            user.setAge(age);
        }
        userRepo.save(user);
        return true;
    }

    public boolean userPOST(String name,String surname, int age){
        User user = new User();
        boolean isUserCreated = true;
        if (null != name && !name.equals("")){
            user.setName(name);
        }
        else{
            isUserCreated = false;
            return isUserCreated;
        }
        if (null != surname && !surname.equals("")){
            user.setSurname(surname);
        }
        else{
            isUserCreated = false;
            return isUserCreated;
        }
        if (age >= 0){
            user.setAge(age);
        }
        else{
            isUserCreated = false;
            return isUserCreated;
        }
        userRepo.saveAndFlush(user);
        return isUserCreated;
    }

    public boolean userDelete(long id){
        User user = userGetByID(id);
        if (user != null){
            userRepo.delete(user);
            return true;
        }
        return false;
    }






    public UserRepo getUserRepo() {
        return userRepo;
    }

    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }


}
