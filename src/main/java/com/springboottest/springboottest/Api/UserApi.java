package com.springboottest.springboottest.Api;



import com.springboottest.springboottest.Model.User;
import com.springboottest.springboottest.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class UserApi {

    private UserService userService;


    @Autowired
    public UserApi(UserService userService) {
        this.userService = userService;
    }


// Directly userRepo commands without userService, it will work if UserRepo userRepo is added and autowired

//    @RequestMapping(value = "/user", method = RequestMethod.POST)
//    public boolean createUser(@RequestParam("name") String name,@RequestParam("surname") String surname,
//                           @RequestParam("age") int age){
//        User user = new User();
//        user.setAge(age);
//        user.setSurname(surname);
//        user.setName(name);
//        userRepo.saveAndFlush(user);
//        return true;
//    }
//    @RequestMapping(value = "/userGetAll", method = RequestMethod.GET)
//    public List<User> getUser(){
//        List<User> all = userRepo.findAll();
//        return all;
//    }
//    @RequestMapping(value = "/userGetByName", method = RequestMethod.GET)
//    public List<User> getUser(@RequestParam("name") String name,@RequestParam("age") int age){
//        List<User> allByName = userRepo.findByNameOrAge(name, age);
//        return allByName;
//    }
//    @RequestMapping(value = "/userGetHigherThanId", method = RequestMethod.GET)
//    public List<User> personGetHigherThanId(@RequestParam("id") Long id){
//        return userRepo.findIds(id);
//    }



//UserServiceCommands

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ArrayList<User> personGet(){
        return userService.userGetAll();
    }
    @RequestMapping(value = "/user/id", method = RequestMethod.GET)
    public User peopleGetByID(@RequestParam("id") long id){
        return userService.userGetByID(id);
    }
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public boolean peoplePOST(@RequestParam("name") String name,@RequestParam("surname") String surname,
                              @RequestParam("age") int age){
        return userService.userPOST(name,surname,age);
    }
    @RequestMapping(value = "/user", method = RequestMethod.DELETE)
    public boolean peopleDelete(@RequestParam("id") long id){
        return userService.userDelete(id);
    }
    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    public boolean peoplePUT(@RequestParam("id") long id, @RequestParam("name") String name,@RequestParam("surname") String surname,
                             @RequestParam("age") int age){
        return userService.userPUT(id,name,surname,age);
    }
}
