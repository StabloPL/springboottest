package com.springboottest.springboottest.Api;

import com.springboottest.springboottest.DTO.CarRepo;
import com.springboottest.springboottest.DTO.UserRepo;
import com.springboottest.springboottest.Model.Car;
import com.springboottest.springboottest.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarApi {
    // CarService should be created
    CarRepo carRepo;
    UserRepo userRepo;

    @Autowired
    public CarApi(CarRepo carRepo, UserRepo userRepo) {
        this.carRepo = carRepo;
        this.userRepo = userRepo;
    }

    @RequestMapping(value = "/testMethodCarAdding", method = RequestMethod.GET)
    public boolean createPet(){
        Car car = new Car();
        car.setName("Car name");
        carRepo.saveAndFlush(car);
        User user = new User();
        user.setName("User");
        car.setUser(user);
        userRepo.saveAndFlush(user);
        return true;
    }
}
