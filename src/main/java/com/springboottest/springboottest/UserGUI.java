package com.springboottest.springboottest;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

@SpringUI
public class UserGUI extends UI {

    @Override
    protected void init(VaadinRequest request) {
        setContent(new Button("Click me!", e ->
                Notification.show("There is no GUI, please use Postman to check how it works")));
    }
}